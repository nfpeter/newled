# -*- coding: UTF-8 -*-

# Pygame-Modul importieren.
import pygame
import socket

# Überprüfen, ob die optionalen Text- und Sound-Module geladen werden konnten.
if not pygame.font: print('Fehler pygame.font Modul konnte nicht geladen werden!')
if not pygame.mixer: print('Fehler pygame.mixer Modul konnte nicht geladen werden!')

UDP_IP = "127.0.0.1"
UDP_PORT = 4210
STRIP_LED_COUNT = 297


def main():
    # Initialisieren aller Pygame-Module und    
    # Fenster erstellen (wir bekommen eine Surface, die den Bildschirm repräsentiert).
    pygame.init()
    screen = pygame.display.set_mode((176, 432))
    
    # Titel des Fensters setzen, Mauszeiger nicht verstecken und Tastendrücke wiederholt senden.
    pygame.display.set_caption("Lampensimulator")
    pygame.mouse.set_visible(1)
    pygame.key.set_repeat(1, 30)
    
    # Clock-Objekt erstellen, das wir benötigen, um die Framerate zu begrenzen.
    clock = pygame.time.Clock()
    
    ledpic = pygame.image.load('led.png')
    
    # Init Socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind((UDP_IP, UDP_PORT))
    sock.setblocking(0)
    data =''
    address = ''
    
    # Die Schleife, und damit unser Spiel, läuft solange running == True.
    running = True
    while running:
        try:
            data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
        except socket.error:
            pass
        
        if len(data) == STRIP_LED_COUNT*3:
            # Framerate auf 30 Frames pro Sekunde beschränken.
            # Pygame wartet, falls das Programm schneller läuft.
            clock.tick(30)
            
            # screen-Surface mit Schwarz (RGB = 0, 0, 0) füllen.
            screen.fill((0, 0, 0))
            
            # LEDs einzeichnen
            for x in range(0, STRIP_LED_COUNT):
                #pygame.draw.rect(screen, (x%255,0,0), ((x%11)*16,(x/11)*16,16,16), 0)
                #pygame.draw.rect(screen, (data[x*3+0].encode(),data[x*3+1].encode(),data[x*3+2].encode()), ((x%11)*16,(x/11)*16,16,16), 0)
                pygame.draw.rect(screen, (ord(data[x*3+1]),ord(data[x*3+0]),ord(data[x*3+2])), ((x%11)*16,(x/11)*16,16,16), 0)
                screen.blit(ledpic, ((x%11)*16,(x/11)*16))
            
            # Orientierungslinien
            pygame.draw.line(screen, (255,255,255), (44, 0), (44, 431))
            pygame.draw.line(screen, (255,255,255), (132, 0), (132, 431))
            pygame.draw.line(screen, (255,255,255), (0, 144), (175, 144))
            pygame.draw.line(screen, (255,255,255), (0, 288), (175, 288))
            
            # Inhalt von screen anzeigen.
            pygame.display.flip()
        
        # Alle aufgelaufenen Events holen und abarbeiten.
        for event in pygame.event.get():
            # Spiel beenden, wenn wir ein QUIT-Event finden.
            if event.type == pygame.QUIT:
                running = False
                
            # Wir interessieren uns auch für "Taste gedrückt"-Events.
            if event.type == pygame.KEYDOWN:
                # Wenn Escape gedrückt wird, posten wir ein QUIT-Event in Pygames Event-Warteschlange.
                if event.key == pygame.K_ESCAPE:
                    pygame.event.post(pygame.event.Event(pygame.QUIT))
        
        
# Überprüfen, ob dieses Modul als Programm läuft und nicht in einem anderen Modul importiert wird.
if __name__ == '__main__':
    # Unsere Main-Funktion aufrufen.
    main()
