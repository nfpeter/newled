import socket
from time import sleep
from random import randint

#UDP_IP = "192.168.0.120"    # Lampe
UDP_IP = "127.0.0.1"    # Lampensimulator
UDP_PORT = 4210
STRIP_LED_COUNT = 297

framecount = 0

print "UDP target IP:", UDP_IP
print "UDP target port:", UDP_PORT

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP

# Prepare list
ledlist = []
for x in range(0, STRIP_LED_COUNT*3):
    ledlist.append(0)

while True:
    # Fade all LEDs
    for x in range(0, STRIP_LED_COUNT*3):
        if ledlist[x] >= 10:
            ledlist[x] = ledlist[x] - 10
        else:
            ledlist[x] = 0
    
    # Light up LED
    ledlist[framecount*3+1] = 255  # +0 for green, +1 for red, +2 for blue
    
    # Inkrement Frame Counter
    framecount = framecount + 1
    if framecount >= STRIP_LED_COUNT:
        framecount = 0

    key = ''
    for c in ledlist:
        key = key + chr(c)
    
    sock.sendto(key, (UDP_IP, UDP_PORT))
    sleep(0.03)
