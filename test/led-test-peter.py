import socket
from time import sleep
from random import randint

#UDP_IP = "127.0.0.1"
#UDP_PORT = 5005
#UDP_IP = "192.168.0.119"
#UDP_IP = "192.168.0.120"    # Lampe
UDP_IP = "127.0.0.1"    # Lampensimulator
UDP_PORT = 4210
#STRIP_LED_COUNT = 240
STRIP_LED_COUNT = 297


#key = ''.join(chr(x) for x in [0x54, 0x6F, 0x6C, 0x6C, 0x65, 0x20, 0x57, 0x75, 0x72, 0x73, 0x74, 0x21, 0x00])
#key = ''.join(chr((x%10)+48) for x in range(0, 1000))
#key = ''.join(chr((x%10)+48)+chr((x%10)+48)+chr((x%10)+48) for x in range(0, STRIP_LED_COUNT))


print "UDP target IP:", UDP_IP
print "UDP target port:", UDP_PORT

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP




while True:
    for brightness in range(0, 65):
        #                 Green     Red       Blue
        #key = ''.join(chr(brightness*4)+chr(0xA0)+chr(0x00) for x in range(0, STRIP_LED_COUNT))
        #key = ''.join(chr(randint(0, 255))+chr(randint(0, 255))+chr(randint(0, 255)) for x in range(0, STRIP_LED_COUNT))
        key = ''.join( chr((10*brightness)/64) + chr((randint(0, 255)*brightness)/64) + chr((randint(0, 255)*brightness)/64) for x in range(0, STRIP_LED_COUNT))
        sock.sendto(key, (UDP_IP, UDP_PORT))
        sleep(1)
        print "LED:" + str(key)
    for brightness in range(0, 65):
        #                 Green     Red       Blue
        #key = ''.join(chr(brightness*4)+chr(0xA0)+chr(0x00) for x in range(0, STRIP_LED_COUNT))
        #key = ''.join(chr(randint(0, 255))+chr(randint(0, 255))+chr(randint(0, 255)) for x in range(0, STRIP_LED_COUNT))
        key = ''.join( chr((255*(64-brightness))/64) + chr((randint(0, 255)*(64-brightness))/64) + chr((randint(0, 255)*(64-brightness))/64) for x in range(0, STRIP_LED_COUNT))
        sock.sendto(key, (UDP_IP, UDP_PORT))
        sleep(0.03)
