import socket
from time import sleep
from random import randint

#UDP_IP = "192.168.0.120"    # Lampe
UDP_IP = "127.0.0.1"    # Lampensimulator
UDP_PORT = 4210
STRIP_LED_COUNT = 297

framecount = 0

print "UDP target IP:", UDP_IP
print "UDP target port:", UDP_PORT

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP

#List class
class leds():
    def __init__(self):
        self.ledlist = []
        for x in range(0, STRIP_LED_COUNT*3):
            self.ledlist.append(0)
    def to_string(self):
        self.key = ''
        for c in self.ledlist:
            self.key = self.key + chr(c)
        return self.key
    def fade(self,position):
        if self.ledlist[position] >= 10:
            self.ledlist[position] = self.ledlist[position] - 10
        else:
            self.ledlist[position] = 0
        return self
    def set_value(self,value,position):
        if position is not None:
            self.ledlist[position] = value 
        return self


# Init LED list
leds = leds()

### MAIN
while True:
    # Fade all LEDs
    for x in range(0, STRIP_LED_COUNT*3):
        leds.set_value(255,x)

    sock.sendto(leds.to_string(), (UDP_IP, UDP_PORT))
    sleep(0.03)

    for x in range(0, STRIP_LED_COUNT*3):
        leds.set_dark(x)

    sock.sendto(leds.to_string(), (UDP_IP, UDP_PORT))    
