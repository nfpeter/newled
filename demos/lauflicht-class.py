import socket
from time import sleep
from random import randint

#UDP_IP = "192.168.0.120"    # Lampe
UDP_IP = "127.0.0.1"    # Lampensimulator
UDP_PORT = 4210
STRIP_LED_COUNT = 297

framecount = 0

print "UDP target IP:", UDP_IP
print "UDP target port:", UDP_PORT

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP

#List class
class leds():
    def __init__(self):
        self.ledlist = []
        for x in range(0, STRIP_LED_COUNT*3):
            self.ledlist.append(0)
    def to_string(self):
        self.key = ''
        for c in self.ledlist:
            self.key = self.key + chr(c)
        return self.key
    def fade(self,position):
        if self.ledlist[position] >= 10:
            self.ledlist[position] = self.ledlist[position] - 10
        else:
            self.ledlist[position] = 0
        return self
    def set_light(self,position,value):
        self.ledlist[position] = value 
        return self


# Init LED list
leds = leds()

### MAIN
while True:
    # Fade all LEDs
    for x in range(0, STRIP_LED_COUNT*3):
        leds.fade(x)
    
    # Light up LED
    leds.set_light(framecount*3+2,255)  # +0 for green, +1 for red, +2 for blue
    
    # Inkrement Frame Counter
    framecount = framecount + 1
    if framecount >= STRIP_LED_COUNT:
        framecount = 0

    sock.sendto(leds.to_string(), (UDP_IP, UDP_PORT))
    sleep(0.03)
