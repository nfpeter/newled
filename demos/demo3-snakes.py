import socket
import led 
from time import sleep
from random import randint

#UDP_IP = "192.168.0.120"    # Lampe
UDP_IP = "127.0.0.1"    # Lampensimulator
UDP_PORT = 4210
STRIP_LED_COUNT = 297

framecount = 0

print "UDP target IP:", UDP_IP
print "UDP target port:", UDP_PORT

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP

#LED class
# 11 LEDs pro Reihe
# 27 Reihen
# 297 LEDs


# Helper function
def leds_to_string(ledlist):
    keylist = []

    #fill keylist with "dark leds"
    for x in range(0, STRIP_LED_COUNT):
        keylist.append(chr(0) + chr(0) +  chr(0))

    #overwrite led at led position with led color
    for cled in ledlist:
        keylist[cled.pos] = cled.to_string()

    return ''.join(keylist) #convert list to string

###create leds
snake1 = []
snake2 = []
snake3 = []
snake4 = []

for x in range(0,7):
    snake1.append(led.led(110, 155-x*12, 255-x*12,0+x))
    snake2.append(led.led(110, 155-x*12, 255-x*12,0+x))
    snake3.append(led.led(110-x*12, 155, 255-x*12,290+x))
    snake4.append(led.led(110-x*12, 155, 255-x*12,290+x))


# BLOOD RAIN
### MAIN
while True:
    framecount = framecount + 1

    map(lambda x: x.left(),snake1)
    map(lambda x: x.right(),snake2)
    map(lambda x: x.left(),snake4)
    map(lambda x: x.right(),snake3)

    if framecount%30 == 0:
        if randint(0, 4) == 0:
            if snake2[0].pos>11:
                map(lambda x: x.up(),snake2)
            else:
                map(lambda x: x.down(),snake3)  
        else:
            if snake2[0].pos<285:
                map(lambda x: x.down(),snake2)
            else:
                map(lambda x: x.up(),snake2)

    if framecount%20 == 0:
        if randint(0, 3) == 0:
            if snake1[0].pos<285:
                map(lambda x: x.down(),snake1)
            else:
                map(lambda x: x.up(),snake4)      
        else:
            map(lambda x: x.up(),snake1)   

    sock.sendto(leds_to_string(led.ledlist), (UDP_IP, UDP_PORT))
    sleep(0.08)