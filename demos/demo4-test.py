import socket
import led 
from time import sleep
from random import randint

#UDP_IP = "192.168.0.120"    # Lampe
UDP_IP = "127.0.0.1"    # Lampensimulator
UDP_PORT = 4210
STRIP_LED_COUNT = 297

framecount = 0

print "UDP target IP:", UDP_IP
print "UDP target port:", UDP_PORT

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP

#LED class
# 11 LEDs pro Reihe
# 27 Reihen
# 297 LEDs


# Helper function
def leds_to_string(ledlist):
    keylist = []

    #fill keylist with "dark leds"
    for x in range(0, STRIP_LED_COUNT):
        keylist.append(chr(0) + chr(0) +  chr(0))

    #overwrite led at led position with led color
    for cled in ledlist:
        keylist[cled.pos] = cled.to_string()

    return ''.join(keylist) #convert list to string

###create leds

for x in range(0,297):
    led.led(255,255,255,x)

# BLOOD RAIN
### MAIN
i=0
color=0
while True:
    led.led(color,color,color,i)
    #map(lambda x: x.down(),led.ledlist)   
    sock.sendto(leds_to_string(led.ledlist), (UDP_IP, UDP_PORT))
    sleep(0.01)
    i = i+1
    if i>=297:
        if color == 0:
            color = 255
        else:
            color = 0    
        i = 0
