# Init LED list
ledlist = []

#LED class
# 11 LEDs pro Reihe
# 27 Reihen
# 297 LEDs
class led():
    def __init__(self,red,green,blue,pos):
        self.red = red
        self.green = green
        self.blue = blue
        self.pos = pos
        ledlist.append(self)
    def to_string(self):
        #   1.Green     2.Red       3.Blue
        return chr(self.green) + chr(self.red) +  chr(self.blue)
    def kill(self):
        ledlist.remove(self)
    def left(self):
        if self.pos%11 == 0:
            self.pos = self.pos + 10 #return to begin
        else:    
            self.pos = self.pos - 1
        return self     
    def right(self):
        if (self.pos+1)%11 == 0:
            self.pos = self.pos - 10 #return to begin
        else:    
            self.pos = self.pos + 1
        return self   
    def down(self):
        if self.pos+11 > 296:
            self.pos = self.pos - 296 + 10  #return to bottom
        else:    
            self.pos = self.pos + 11
        return self         
    def up(self):
        if self.pos-11 < 0:
            self.pos = self.pos + 296 - 10  #return to bottom
        else:    
            self.pos = self.pos - 11
        return self            
    def light_up(self):
        if self.red > 255:
            self.red = self.red+1  
        return self 