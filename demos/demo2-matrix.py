import socket
import led 
from time import sleep
from random import randint

#UDP_IP = "192.168.0.120"    # Lampe
UDP_IP = "127.0.0.1"    # Lampensimulator
UDP_PORT = 4210
STRIP_LED_COUNT = 297

framecount = 0

print "UDP target IP:", UDP_IP
print "UDP target port:", UDP_PORT

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP

#LED class
# 11 LEDs pro Reihe
# 27 Reihen
# 297 LEDs


# Helper function
def leds_to_string(ledlist):
    keylist = []

    #fill keylist with "dark leds"
    for x in range(0, STRIP_LED_COUNT):
        keylist.append(chr(0) + chr(0) +  chr(0))

    #overwrite led at led position with led color
    for cled in ledlist:
        keylist[cled.pos] = cled.to_string()

    return ''.join(keylist) #convert list to string

###create leds
fast = []
slow = []
slower = []

for x in range(0,randint(7, 14)):
    fast.append(led.led(30, 197-randint(0, 100), 3,randint(0, 296)))

for x in range(0,randint(10, 20)):
    slow.append(led.led(30, 197-randint(0, 100), 3,randint(0, 296)))    

for x in range(0,randint(7, 14)):
    slower.append(led.led(30, 197-randint(0, 100), 3,randint(0, 296)))   

# BLOOD RAIN
### MAIN
while True:
    framecount = framecount + 1

    map(lambda x: x.down(),fast)

    if framecount%2 == 0:
        map(lambda x: x.down(),slow)

    if framecount%3 == 0:
        map(lambda x: x.down(),slower)

    sock.sendto(leds_to_string(led.ledlist), (UDP_IP, UDP_PORT))
    sleep(0.08)