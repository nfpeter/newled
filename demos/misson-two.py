import socket
import led 
from time import sleep
from random import randint

#UDP_IP = "192.168.0.120"    # Lampe
UDP_IP = "127.0.0.1"    # Lampensimulator
UDP_PORT = 4210
STRIP_LED_COUNT = 297

framecount = 0

print "UDP target IP:", UDP_IP
print "UDP target port:", UDP_PORT

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP

#LED class
# 11 LEDs pro Reihe
# 27 Reihen
# 297 LEDs


# Helper function
def leds_to_string(ledlist):
    keylist = []

    #fill keylist with "dark leds"
    for x in range(0, STRIP_LED_COUNT):
        keylist.append(chr(0) + chr(0) +  chr(0))

    #overwrite led at led position with led color
    for cled in ledlist:
        keylist[cled.pos] = cled.to_string()

    return ''.join(keylist) #convert list to string



# new LED
l = led.led(255,0,0,296)
#ledlist.append(a)
#b = led(255,255,255,200)
#ledlist.append(b)

#ledlist.append(led(255,0,255,2))
#ledlist.append(led(255,200,255,4))
#ledlist.append(led(255,0,255,2))
#ledlist.append(led(100,0,255,6))
#ledlist.append()


### MAIN
while True:
    # Fade all LEDs
    l.right()

    sock.sendto(leds_to_string(led.ledlist), (UDP_IP, UDP_PORT))
    sleep(0.1)
    print l.pos

    #for x in range(0, STRIP_LED_COUNT*3):
     #   leds.set_dark(x)

    #sock.sendto(leds.to_string(), (UDP_IP, UDP_PORT))    

